#ifndef TIMER_H
#define TIMER_H

void timer2_init(int interval);
void delayms(int ms);

long millis(void);

void tenMicroSeconds(void);
void sixtyMicroSeconds(void);

#endif TIMER_H