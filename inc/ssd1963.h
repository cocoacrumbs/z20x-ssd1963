#define fontdatatype const unsigned char

#define swap(type, i, j) {type t = i; i = j; j = t;}

/* ************************************************************************ */

typedef struct current_font_t
{
	unsigned char*  font;
	unsigned char   x_size;
	unsigned char   y_size;
	unsigned char   offset;
	unsigned char   numchars;
} current_font;


typedef struct SSD1963_T
{
    unsigned int    sizeX;
    unsigned int    sizeY;

    unsigned char   frontColor_Red;
    unsigned char   frontColor_Green;
    unsigned char   frontColor_Blue;

    unsigned char   backColor_Red;
    unsigned char   backColor_Green;
    unsigned char   backColor_Blue;

    current_font	currentFont;
} SSD1963;


enum colorFast { WHITE, RED, GREEN, BLUE, YELLOW, CYAN, MAGENTA, BLACK };

/* ************************************************************************ */

void ssd1963_setup(SSD1963* ssd1963);

/* ************************************************************************ */

void ssd1963_setXY(unsigned int x1, 
                   unsigned int y1, 
                   unsigned int x2, 
                   unsigned int y2);

void ssd1963_resetXY(SSD1963* ssd1963);

/* ************************************************************************ */

void ssd1963_setGammaCurve(SSD1963* ssd1963,
                           unsigned char gammaCurve);

void ssd1963_setFrontColor(SSD1963* ssd1963,
                           unsigned char red,
                           unsigned char green,
                           unsigned char blue);

void ssd1963_setBackgroundColor(SSD1963* ssd1963,
                                unsigned char red,
                                unsigned char green,
                                unsigned char blue);

/* ************************************************************************ */

void ssd1963_setFont(SSD1963* ssd1963,
                     unsigned char* font);

/* ************************************************************************ */

void ssd1963_clrScr(SSD1963* ssd1963);

/* ************************************************************************ */

void ssd1963_drawPixel(SSD1963* ssd1963,
                       int x, 
                       int y);

/* ************************************************************************ */

void ssd1963_drawHLine(SSD1963* ssd1963,
                       unsigned int x,
                       unsigned int y,
                       unsigned int length);

void ssd1963_drawVLine(SSD1963* ssd1963,
                       unsigned int x,
                       unsigned int y,
                       unsigned int length);

void ssd1963_drawLine(SSD1963* ssd1963,
                      unsigned int x1,
                      unsigned int y1,
                      unsigned int x2,
                      unsigned int y2);

/* ************************************************************************ */

void ssd1963_drawRectangle(SSD1963* ssd1963,
                           unsigned int x,
                           unsigned int y,
                           unsigned int width,
                           unsigned int height);

void ssd1963_drawRoundedRectangle(SSD1963* ssd1963,
                                  int x, 
                                  int y, 
                                  unsigned int width,
                                  unsigned int height);

void ssd1963_fillRectangle(SSD1963* ssd1963,
                           int x,
                           int y, 
                           unsigned int width,
                           unsigned int height);

void ssd1963_fillRoundedRectangle(SSD1963* ssd1963,
                                  int x, 
                                  int y,
                                  unsigned int width,
                                  unsigned int height);

/* ************************************************************************ */

void ssd1963_drawCircle(SSD1963* ssd1963,
                        int x, 
                        int y, 
                        int radius);
                        
/* ************************************************************************ */

void ssd1963_printChar(SSD1963* ssd1963,
                       unsigned char character,
                       int x,
                       int y);

void ssd1963_print(SSD1963* ssd1963,
                   char *string, 
                   int x, 
                   int y);

/* ************************************************************************ */

/* The following print routines print considerably faster but at the cost of 
   some flexibility. 
   Fonts have to be 8 pixels white and the fore/background colors are not used. 
   Instead, there are 8 print routines to print in the 8 basic colors. 
*/
void ssd1963_printCharFast_WHITE(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y);

void ssd1963_printCharFast_RED(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y);

void ssd1963_printCharFast_GREEN(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y);

void ssd1963_printCharFast_BLUE(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y);

void ssd1963_printCharFast_YELLOW(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y);

void ssd1963_printCharFast_CYAN(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y);

void ssd1963_printCharFast_MAGENTA(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y);
                                 
void ssd1963_printCharFast_BLACK(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y);

void ssd1963_printCharFast(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y,
                                 enum colorFast color);

void ssd1963_printFast(SSD1963* ssd1963,
                       char *string, 
                       int x, 
                       int y,
                       enum colorFast color);
                      
/* ************************************************************************ */