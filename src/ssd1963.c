#include <stdlib.h>
#include <string.h>

#include "timer.h"
#include "ssd1963.h"

/* ************************************************************************ */

static unsigned char  *writeCOM    = (unsigned char*)0x400000;
static unsigned char  *writeDATA   = (unsigned char*)0x400001;

/* ************************************************************************ */

void ssd1963_fast_fill(SSD1963* ssd1963, 
                       long pix)
{
    int     ctr     = 0;
	long    blocks  = pix / 8;

	for (ctr=0; ctr<blocks; ctr++)
	{
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;

        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;

        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;

        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;

        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;

        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;

        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;

        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
	} /* end for */

	if ((pix % 8) != 0)
    {
		for (ctr=0; ctr<(pix % 8)+1; ctr++)
		{
            *writeDATA = ssd1963->frontColor_Red;
            *writeDATA = ssd1963->frontColor_Green;
            *writeDATA = ssd1963->frontColor_Blue;
        } /* end for */
    } /* end if */
} /* end ssd1963_fast_fill */

/* ************************************************************************ */

void ssd1963_setup(SSD1963* ssd1963)
{
    if (ssd1963 != NULL)
    {
        ssd1963->sizeX                  = 799;    // 800 - 1
        ssd1963->sizeY                  = 479;    // 480 - 1

        ssd1963->frontColor_Red         = 0xFF;   // White
        ssd1963->frontColor_Green       = 0xFF;
        ssd1963->frontColor_Blue        = 0xFF;

        ssd1963->backColor_Red          = 0x00;   // Black
        ssd1963->backColor_Green        = 0x00;
        ssd1963->backColor_Blue         = 0x00;

        ssd1963->currentFont.font       = NULL;
        ssd1963->currentFont.x_size     = 0;
        ssd1963->currentFont.y_size     = 0;
        ssd1963->currentFont.offset     = 0;
        ssd1963->currentFont.numchars   = 0;
    }
    else
    {
        return;
    } /* end if */

    // Configure SSD1963’s PLL frequency
    // ---------------------------------
    // VCO = Input clock x (M + 1)
    // PLL frequency = VCO / (N + 1)
    //
    // * Notes :
    // 1. 250MHz < VCO < 800MHz
    //    PLL frequency < 110MHz
    // 2. For a 10MHz input clock to obtain 100MHz PLL frequency, user cannot program M = 19 and N = 1. 
    //    The closet setting in this situation is setting M=29 and N=2, where 10 x 30 / 3 = 100MHz.
    // 3. Before PLL is locked, SSD1961/2/3 is operating at input clock frequency (e.g. 10MHz), registers
    //     programming cannot be set faster than half of the input clock frequency (5M words/s in this example). 
    //
	// The max PLL freq is around 120MHz. To obtain 120MHz as the PLL freq assuming input clock frequency is 10MHz
    //      Multiplier N = 35 (0x23), VCO (>250MHz)= OSC*(N+1), VCO = 360MHz
    //      Divider M = 2 (0x02), PLL = 360/(M+1) = 120MHz
    *writeCOM  = 0xE2;              // Set PLL with OSC = 10MHz (hardware)
    *writeDATA = 0x23;              // Multiplier N = 35 (0x23) for 10MHz crystal (N=0x36 for 6.5MHz crystal)
    *writeDATA = 0x02;              // Divider M = 2 (0x02)
    *writeDATA = 0x54;              // Write a dummy byte

    *writeCOM  = 0xE0;              // Turn on PLL
    *writeDATA = 0x01;              // 1 == enable PLL

	delayms(10);                    // Give PLL some time to stabilize

    *writeCOM  = 0xE0;              // Start PLL command again
    *writeDATA = 0x03;              // Use PLL output as system clock	

	delayms(10);                    // Allow some time to do the switch

    *writeCOM  = 0x01;              // software reset

	delayms(10);                    // Give display some time to 'reboot'

    // Configure the dot clock frequency
    // ---------------------------------
	// Set the LSHIFT freq, i.e. the DCLK with PLL freq 120MHz as set previously
	// The typical DCLK for a TY700TFT800480 LCD is 30MHz (see datasheet)
    //
	// 30MHz = 120MHz * (LCDC_FPR + 1) / 2^20
	// LCDC_FPR = ((30/120) * 2^20) - 1
    // LCDC_FPR = 262.143
    // LCDC_FPR = 0x03FFFF

    *writeCOM =  0xE6;              // set_lshift_freq command (Set the LSHIFT (pixel clock) frequency)
    *writeDATA = 0x03;
    *writeDATA = 0xFF;
    *writeDATA = 0xFF;

    // Set panel mode for a 7" TFT LCD screen
    *writeCOM  = 0xB0;              // set_lcd_mode command
    *writeDATA = 0x10;              // set 18-bit/24-bit for 7" panel TY700TFT800480
    *writeDATA = 0x00;              // TFT mode
    *writeDATA = 0x03;              // Set horizontal display resolution to 800 - 1 = 799 = 0x031F
    *writeDATA = 0x1F;              
    *writeDATA = 0x01;              // Set vertical display resolution to 480 - 1 = 479 = 0x01DF
    *writeDATA = 0xDF;
    *writeDATA = 0x00;              // RGB sequence	(serial TFT interface only)

    // Set horizontal period
    *writeCOM  = 0xB4;              // set_hori_period command
    *writeDATA = 0x03;              // Set horizontal total period: 928 (0x3A0)
    *writeDATA = 0xA0;
    *writeDATA = 0x00;              // Horizontal Sync Pulse Start Position = HPS pixels = 46 (0x2E)
    *writeDATA = 0x2E;
    *writeDATA = 0x30;              // Horizontal Sync Pulse Width = (HPW + 1) pixels = 48 (0x30)
    *writeDATA = 0x00;              // Horizontal Display Period Start Position = LPS pixels = 15 (0x0F)
    *writeDATA = 0x0F;
    *writeDATA = 0x00;              // Horizontal sync pulse subpixel start position for serial TFT interface = 0 (0x00)

    // Set vertical period
    *writeCOM  = 0xB6;              // set_vert_period command
    *writeDATA = 0x02;              // Vertical total (display + non-display) period in lines = 525 (0x20D)
    *writeDATA = 0x0D;
    *writeDATA = 0x00;              // Non-display period in lines between the start of the frame and the first display data in line (VPS) = 16 (0x10)
    *writeDATA = 0x10;
    *writeDATA = 0x10;              // Vertical sync pulse width (VPW) = 16 (0x10)
    *writeDATA = 0x00;              // Vertical Display Period Start Position (FPS) = 8 (0x08)
    *writeDATA = 0x08;

    // Configure the GPIO
    *writeCOM  = 0xB8;              // set_gpio_conf command
    *writeDATA = 0x0E;              // Config GPIO3-1 as output, GPIO0 as input
    *writeDATA = 0x01;              // shutdown control on GPIO0 not used
    *writeCOM  = 0xBA;              // set_gpio_value command
    *writeDATA = 0x0E;              // Set GPIO3-1 as high

    // Setup the addressing mode to rotate mode
    *writeCOM  = 0x36;              // set_address_mode command
    *writeDATA = 0x03;              // A[7] : Page address order (POR = 0)
                                    //     This bit controls the order that pages of data are transferred from the host processor to the SSD1963’s frame buffer.
                                    //     0 Top to bottom, pages transferred from SP (Start Page) to EP (End Page).
                                    //     1 Bottom to top, pages transferred from EP (End Page) to SP (Start Page).
                                    // A[6] : Column address order (POR = 0)
                                    //     This bit controls the order that columns of data are transferred from the host processor to the SSD1963’s frame buffer.
                                    //     0 Left to right, columns transferred from SC (Start Column) to EC (End Column).
                                    //     1 Right to left, columns transferred from EC (End Column) to SC (Start Column).
                                    // A[5] : Page / Column order (POR = 0)
                                    //     This bit controls the order that columns of data are transferred from the host processor to the SSD1963’s frame buffer.
                                    //     0 Normal mode
                                    //     1 Reverse mode
                                    // A[4] : Line address order (POR = 0)
                                    //     This bit controls the display panel’s horizontal line refresh order. The image shown on the display panel is unaffected,
                                    //     regardless of the bit setting.
                                    //     0 LCD refresh from top line to bottom line.
                                    //     1 LCD refresh from bottom line to top line.
                                    // A[3] : RGB / BGR order (POR = 0)
                                    //     This bit controls the RGB data order transferred from the SSD1963’s frame buffer to the display panel.
                                    //     0 RGB
                                    //     1 BGR
                                    // A[2] : Display data latch data (POR = 0)
                                    //     This bit controls the display panel’s vertical line data latch order. The image shown on the display panel is unaffected,
                                    //     regardless of the bit setting.
                                    //     0 LCD refresh from left side to right side
                                    //     1 LCD refresh from right side to left side
                                    // A[1] : Flip Horizontal (POR = 0)
                                    //     This bit flips the image shown on the display panel left to right. No change is made to the frame buffer.
                                    //     0 Normal
                                    //     1 Flipped
                                    // A[0] : Flip Vertical (POR = 0)
                                    //     This bit flips the image shown on the display panel top to bottom. No change is made to the frame buffer.
                                    //     0 Normal
                                    //     1 Flipped

    *writeCOM  = 0xF0;              // set_pixel_data_interface command
    *writeDATA = 0x00;              // 8-bit data interface (3 writes for 24bpp)
                                    // A[2:0] : Pixel Data Interface Format (POR = 101)
                                    // 000 8-bit
                                    // 001 12-bit
                                    // 010 16-bit packed
                                    // 011 16-bit (565 format)
                                    // 100 18-bit
                                    // 101 24-bit
                                    // 110 9-bit
                                    // Others Reserved

    // Configure the frame buffer (LANDSCAPE orientation 7" display with 800 by 480 resolution)
    ssd1963_setXY(0, 0, ssd1963->sizeX, ssd1963->sizeY);  
    
    *writeCOM  = 0x29;              // set_display_on command

    // PWM signal frequency = PLL clock / (256 * (PWMF[7:0] + 1)) / 256
    *writeCOM  = 0xBE;              // Set set_pwm_conf command
    *writeDATA = 0x06;              // PWMF (PWM frequency) 0x06 gives a PWM frequency of 1.782 kHz
    *writeDATA = 0xF0;              // PWM duty cycle (0xF0 == 94%)
    *writeDATA = 0x01;              // [3] PWM controlled by host, [0] PWM enable
    *writeDATA = 0xF0;              // DBC manual brightnes (0xF0 == 94%)
    *writeDATA = 0x00;              // DBC minimum brightness
    *writeDATA = 0x00;              // Brightness prescaler (0x00 == OFF)

    *writeCOM  = 0x2C;              // write_memory_start command
} /* end void */

/* ************************************************************************ */

void ssd1963_setXY(unsigned int x1, 
                   unsigned int y1, 
                   unsigned int x2, 
                   unsigned int y2)
{
    // Setup the frame buffer 'horizontal' addressing range to "x1 to x2"
    *writeCOM  = 0x2A;                      // set_column_address command
    *writeDATA = (unsigned char)(x1>>8);    // Start Column address: x1
    *writeDATA = (unsigned char)(x1%256);
    *writeDATA = (unsigned char)(x2>>8);    // End Column address: x2
    *writeDATA = (unsigned char)(x2%256);
    // Setup the frame buffer horizontal address range to "y1 to y2" 
    *writeCOM  = 0x2B;                      // set_page_address command
    *writeDATA = (unsigned char)(y1>>8);    // Start Page: y1
    *writeDATA = (unsigned char)(y1%256);
    *writeDATA = (unsigned char)(y2>>8);    // End Page: y2
    *writeDATA = (unsigned char)(y2%256);    

    *writeCOM  = 0x2C;                      // write_memory_start command
} /* end ssd1963_setXY */


void ssd1963_resetXY(SSD1963* ssd1963)
{
    ssd1963_setXY(0, 0, ssd1963->sizeX, ssd1963->sizeY);
} /* end ssd1963_resetXY */

/* ************************************************************************ */

void ssd1963_setGammaCurve(SSD1963* ssd1963,
                           unsigned char gammaCurve)
{
    *writeCOM  = 0x26;                      // set_gamma_curve command
    switch (gammaCurve)
    {
        case 0:
            *writeDATA = 0x01;
            break;
        case 1:
            *writeDATA = 0x02;    
            break;
        case 2:
            *writeDATA = 0x04;    
            break;
        case 3:
            *writeDATA = 0x08;    
            break;
        default:
            *writeDATA = 0x00;   
            break;
    } /* end switch */
    *writeCOM  = 0x2C;                      // write_memory_start command
} /* end ssd1963_setGammaCurve */


void ssd1963_setFrontColor(SSD1963* ssd1963,
                           unsigned char red,
                           unsigned char green,
                           unsigned char blue)
{
    ssd1963->frontColor_Red   = red;
    ssd1963->frontColor_Green = green;
    ssd1963->frontColor_Blue  = blue;
} /* end ssd1963_setFrontColor */


void ssd1963_setBackgroundColor(SSD1963* ssd1963,
                                unsigned char red,
                                unsigned char green,
                                unsigned char blue)
{
    ssd1963->backColor_Red   = red;
    ssd1963->backColor_Green = green;
    ssd1963->backColor_Blue  = blue;
} /* end ssd1963_setBackgroundColor */

/* ************************************************************************ */

void ssd1963_setFont(SSD1963* ssd1963,
                     unsigned char* font)
{
    ssd1963->currentFont.font       = font;
    ssd1963->currentFont.x_size     = font[0];
    ssd1963->currentFont.y_size     = font[1];
    ssd1963->currentFont.offset     = font[2];
    ssd1963->currentFont.numchars   = font[3];
} /* end ssd1963_setFont */

/* ************************************************************************ */

void ssd1963_clrScr(SSD1963* ssd1963)
{
    int ctr         = 0;
    int nrOfPixels  = 800 * 480;

    ssd1963_resetXY(ssd1963);

    for (ctr=0; ctr<nrOfPixels/16; ctr++)
    {
        asm("LD	HL,(_writeDATA)");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");

        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");

        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");

        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
    } /* end for */
} /* end ssd1963_clrScr */

/* ************************************************************************ */

void ssd1963_drawPixel(SSD1963* ssd1963,
                       int x, 
                       int y)
{
	ssd1963_setXY(x, y, x, y);

    *writeDATA = ssd1963->frontColor_Red;
    *writeDATA = ssd1963->frontColor_Green;
    *writeDATA = ssd1963->frontColor_Blue;

    ssd1963_resetXY(ssd1963);
} /* end ssd1963_drawPixel */

/* ************************************************************************ */

void ssd1963_drawHLine(SSD1963* ssd1963,
                       unsigned int x,
                       unsigned int y,
                       unsigned int length)
{
    int ctr;

    ssd1963_setXY(x, y, x+length, y);

    for (ctr=0; ctr<length; ctr++)
    {
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    } /* end for */

    ssd1963_resetXY(ssd1963);
} /* end ssd1963_drawHLine */


void ssd1963_drawVLine(SSD1963* ssd1963,
                       unsigned int x,
                       unsigned int y,
                       unsigned int length)
{
    int ctr;

    ssd1963_setXY(x, y, x, y+length);

    for (ctr=0; ctr<length; ctr++)
    {
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    } /* end for */

    ssd1963_resetXY(ssd1963);
} /* end ssd1963_drawVLine */


void ssd1963_drawLine(SSD1963* ssd1963,
                      unsigned int x1,
                      unsigned int y1,
                      unsigned int x2,
                      unsigned int y2)
{
	if (y1==y2)
		ssd1963_drawHLine(ssd1963, x1, y1, x2-x1);
	else if (x1==x2)
		ssd1963_drawVLine(ssd1963, x1, y1, y2-y1);
	else
	{
        unsigned int	dx      = 0;
		unsigned int	dy      = 0;
		short			xstep   = 0;
		short			ystep   = 0;
		int				col     = x1;
        int             row     = y1;

        dx = abs(x1 - x2);
        dy = abs(y2 - y1);
        if (x2 > x1)
            xstep = 1;
        else
            xstep = -1;
        if (y2 > y1)
            ystep = 1;
        else
            ystep = -1;

		if (dx < dy)
		{
			int t = -(dy >> 1);
			while (1)
			{
				ssd1963_setXY(col, row, col, row);
                *writeDATA = ssd1963->frontColor_Red;
                *writeDATA = ssd1963->frontColor_Green;
                *writeDATA = ssd1963->frontColor_Blue;

				if (row == y2)
					break;

				row += ystep;
				t += dx;
				if (t >= 0)
				{
					col += xstep;
					t   -= dy;
				} /* end if */
			} /* end while */
		}
		else
		{
			int t = -(dx >> 1);
			while (1)
			{
				ssd1963_setXY(col, row, col, row);
                *writeDATA = ssd1963->frontColor_Red;
                *writeDATA = ssd1963->frontColor_Green;
                *writeDATA = ssd1963->frontColor_Blue;    
                            
				if (col == x2)
					break;

				col += xstep;
				t += dy;
				if (t >= 0)
				{
					row += ystep;
					t   -= dx;
				} /* end if */
			} /* end while */
		} /* end if */
        ssd1963_resetXY(ssd1963);
    } /* end if */
} /* end ssd1963_drawLine */

/* ************************************************************************ */

void ssd1963_drawRectangle(SSD1963* ssd1963,
                           unsigned int x,
                           unsigned int y,
                           unsigned int width,
                           unsigned int height)
{
    ssd1963_drawHLine(ssd1963, x, y, width);
    ssd1963_drawHLine(ssd1963, x, y+height, width);
    ssd1963_drawVLine(ssd1963, x, y, height);
    ssd1963_drawVLine(ssd1963, x+width, y, height);
} /* end ssd1963_drawRectangle */


void ssd1963_drawRoundedRectangle(SSD1963* ssd1963,
                                  int x, 
                                  int y, 
                                  unsigned int width,
                                  unsigned int height)
{
	if ((width>4) && (height>4))
	{
		ssd1963_drawPixel(ssd1963, x + 1,         y + 1);
		ssd1963_drawPixel(ssd1963, x + width - 1, y + 1);
		ssd1963_drawPixel(ssd1963, x + 1,         y + height - 1);
		ssd1963_drawPixel(ssd1963, x + width - 1, y + height - 1);
		ssd1963_drawHLine(ssd1963, x + 2,         y,          width - 4);
		ssd1963_drawHLine(ssd1963, x + 2,         y + height, width - 4);
		ssd1963_drawVLine(ssd1963, x,             y + 2,      height - 4);
		ssd1963_drawVLine(ssd1963, x + width,     y + 2,      height - 4);
	} /* end if */
} /* end ssd1963_drawRoundedRectangle */


void ssd1963_fillRectangle(SSD1963* ssd1963,
                           int x, 
                           int y, 
                           unsigned int width,
                           unsigned int height)
{
    ssd1963_setXY(x, y, x + width, y + height);
	ssd1963_fast_fill(ssd1963, ((long)width + 1) * ((long)height + 1));
} /* end ssd1963_fillRectangle */


void ssd1963_fillRoundedRectangle(SSD1963* ssd1963,
                                  int x, 
                                  int y,
                                  unsigned int width,
                                  unsigned int height)
{
    int ctr = 0;

	if (width>4 && height>4)
	{
		for (ctr=0; ctr<(height/2)+1; ctr++)
		{
			switch(ctr)
			{
			    case 0:
    				ssd1963_drawHLine(ssd1963, x+2, y+ctr,        width-4);
    				ssd1963_drawHLine(ssd1963, x+2, y+height-ctr, width-4);
				    break;
			    case 1:
				    ssd1963_drawHLine(ssd1963, x+1, y+ctr,        width-2);
    				ssd1963_drawHLine(ssd1963, x+1, y+height-ctr, width-2);
    				break;
			    default:
				    ssd1963_drawHLine(ssd1963, x, y+ctr,        width);
    				ssd1963_drawHLine(ssd1963, x, y+height-ctr, width);
			} /* end switch */
		} /* end for */
	} /* end if */
} /* end ssd1963_fillRoundedRectangle */

/* ************************************************************************ */

void ssd1963_drawCircle(SSD1963* ssd1963,
                        int x, 
                        int y, 
                        int radius)
{
	int f       = 1 - radius;
	int ddF_x   = 1;
	int ddF_y   = -2 * radius;
	int x1      = 0;
	int y1      = radius;

	ssd1963_setXY(x, y + radius, x, y + radius);
    *writeDATA = ssd1963->frontColor_Red;
    *writeDATA = ssd1963->frontColor_Green;
    *writeDATA = ssd1963->frontColor_Blue;

	ssd1963_setXY(x, y - radius, x, y - radius);
    *writeDATA = ssd1963->frontColor_Red;
    *writeDATA = ssd1963->frontColor_Green;
    *writeDATA = ssd1963->frontColor_Blue;

	ssd1963_setXY(x + radius, y, x + radius, y);
    *writeDATA = ssd1963->frontColor_Red;
    *writeDATA = ssd1963->frontColor_Green;
    *writeDATA = ssd1963->frontColor_Blue;

	ssd1963_setXY(x - radius, y, x - radius, y);
    *writeDATA = ssd1963->frontColor_Red;
    *writeDATA = ssd1963->frontColor_Green;
    *writeDATA = ssd1963->frontColor_Blue;
 
	while (x1 < y1)
	{
		if (f >= 0) 
		{
			y1--;
			ddF_y += 2;
			f += ddF_y;
		} /* end if */
		x1++;
		ddF_x += 2;
		f     += ddF_x;    

		ssd1963_setXY(x + x1, y + y1, x + x1, y + y1);
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    
 		ssd1963_setXY(x - x1, y + y1, x - x1, y + y1);
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    
 		ssd1963_setXY(x + x1, y - y1, x + x1, y - y1);
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    
		ssd1963_setXY(x - x1, y - y1, x - x1, y - y1);
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    
 		ssd1963_setXY(x + y1, y + x1, x + y1, y + x1);
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    
 		ssd1963_setXY(x - y1, y + x1, x - y1, y + x1);
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    
 		ssd1963_setXY(x + y1, y - x1, x + y1, y - x1);
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    
 		ssd1963_setXY(x - y1, y - x1, x - y1, y - x1);
        *writeDATA = ssd1963->frontColor_Red;
        *writeDATA = ssd1963->frontColor_Green;
        *writeDATA = ssd1963->frontColor_Blue;
    } /* end if */
    ssd1963_resetXY(ssd1963);
} /* end ssd1963_drawCircle */

/* ************************************************************************ */

void ssd1963_printChar(SSD1963* ssd1963,
                       unsigned char character,
                       int x,
                       int y)
{
    int             temp;
    int             i;
    int             j;
    int             zz;
    unsigned char   ch;

    temp = ((character - ssd1963->currentFont.offset) * ((ssd1963->currentFont.x_size / 8) * ssd1963->currentFont.y_size)) + 4;

    for (j=((ssd1963->currentFont.x_size/8)*ssd1963->currentFont.y_size); j>0; j-=(ssd1963->currentFont.x_size/8))
    {
        ssd1963_setXY(x, 
                      y+(j/(ssd1963->currentFont.x_size/8)), 
                      x+ssd1963->currentFont.x_size-1, 
                      y+(j/(ssd1963->currentFont.x_size/8)));
        for (zz=0; zz<=(ssd1963->currentFont.x_size/8)-1; zz++)
        {
            ch = ssd1963->currentFont.font[temp + zz];
            for (i=7; i>=0; i--)
            {
                if ((ch&(1<<i)) != 0)   
                {
                    *writeDATA = ssd1963->frontColor_Red;
                    *writeDATA = ssd1963->frontColor_Green;
                    *writeDATA = ssd1963->frontColor_Blue;
                } 
                else
                {
                    *writeDATA = ssd1963->backColor_Red;
                    *writeDATA = ssd1963->backColor_Green;
                    *writeDATA = ssd1963->backColor_Blue;
                } /* end if */
            } /* end for */
        } /* end for */
        temp = temp + (ssd1963->currentFont.x_size/8);
    } /* end for */
} /* end ssd1963_printChar */


void ssd1963_print(SSD1963* ssd1963,
                   char *string, 
                   int x, 
                   int y)
{
	int stl;
    int i;

	stl = strlen(string);
	for (i=0; i<stl; i++)
		ssd1963_printChar(ssd1963, (unsigned char)*string++, x + (i*(ssd1963->currentFont.x_size)), y);
} /* end ssd1963_print */

/* ************************************************************************ */

void ssd1963_printCharFast_WHITE(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y)
{
    int             charHeigth  = ssd1963->currentFont.y_size;
    int             offset      = ((character - ssd1963->currentFont.offset) * charHeigth) + 4;
    int             idx;
    int             i;
    unsigned char*  row         = ssd1963->currentFont.font + offset + charHeigth - 1;;

    ssd1963_setXY(x, y, x + 7, y + charHeigth);
    for (idx = 0; idx < charHeigth; idx++)
    {
        if ((*row & 0x80) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x40) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x20) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x10) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x08) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x04) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x02) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x01) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        row--;      
    } /* end for */
} /* end ssd1963_printCharFast_WHITE */


void ssd1963_printCharFast_RED(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y)
{
    int             charHeigth  = ssd1963->currentFont.y_size;
    int             offset      = ((character - ssd1963->currentFont.offset) * charHeigth) + 4;
    int             idx;
    int             i;
    unsigned char*  row         = ssd1963->currentFont.font + offset + charHeigth - 1;;

    ssd1963_setXY(x, y, x + 7, y + charHeigth);
    for (idx = 0; idx < charHeigth; idx++)
    {
        if ((*row & 0x80) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x40) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x20) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x10) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x08) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x04) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x02) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x01) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        row--;      
    } /* end for */
} /* end ssd1963_printCharFast_RED */


void ssd1963_printCharFast_GREEN(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y)
{
    int             charHeigth  = ssd1963->currentFont.y_size;
    int             offset      = ((character - ssd1963->currentFont.offset) * charHeigth) + 4;
    int             idx;
    int             i;
    unsigned char*  row         = ssd1963->currentFont.font + offset + charHeigth - 1;;

    ssd1963_setXY(x, y, x + 7, y + charHeigth);
    for (idx = 0; idx < charHeigth; idx++)
    {
        if ((*row & 0x80) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x40) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x20) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x10) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x08) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x04) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x02) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x01) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        row--;      
    } /* end for */
} /* end ssd1963_printCharFast_GREEN */


void ssd1963_printCharFast_BLUE(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y)
{
    int             charHeigth  = ssd1963->currentFont.y_size;
    int             offset      = ((character - ssd1963->currentFont.offset) * charHeigth) + 4;
    int             idx;
    int             i;
    unsigned char*  row         = ssd1963->currentFont.font + offset + charHeigth - 1;;

    ssd1963_setXY(x, y, x + 7, y + charHeigth);
    for (idx = 0; idx < charHeigth; idx++)
    {
        if ((*row & 0x80) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x40) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x20) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x10) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x08) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x04) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x02) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x01) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        row--;      
    } /* end for */
} /* end ssd1963_printCharFast_BLUE */


void ssd1963_printCharFast_YELLOW(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y)
{
    int             charHeigth  = ssd1963->currentFont.y_size;
    int             offset      = ((character - ssd1963->currentFont.offset) * charHeigth) + 4;
    int             idx;
    int             i;
    unsigned char*  row         = ssd1963->currentFont.font + offset + charHeigth - 1;;

    ssd1963_setXY(x, y, x + 7, y + charHeigth);
    for (idx = 0; idx < charHeigth; idx++)
    {
        if ((*row & 0x80) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x40) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x20) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x10) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x08) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x04) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x02) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x01) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        row--;      
    } /* end for */
} /* end ssd1963_printCharFast_YELLOW */


void ssd1963_printCharFast_CYAN(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y)
{
    int             charHeigth  = ssd1963->currentFont.y_size;
    int             offset      = ((character - ssd1963->currentFont.offset) * charHeigth) + 4;
    int             idx;
    int             i;
    unsigned char*  row         = ssd1963->currentFont.font + offset + charHeigth - 1;;

    ssd1963_setXY(x, y, x + 7, y + charHeigth);
    for (idx = 0; idx < charHeigth; idx++)
    {
        if ((*row & 0x80) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x40) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x20) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x10) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x08) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x04) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x02) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x01) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        row--;      
    } /* end for */
} /* end ssd1963_printCharFast_CYAN */


void ssd1963_printCharFast_MAGENTA(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y)
{
    int             charHeigth  = ssd1963->currentFont.y_size;
    int             offset      = ((character - ssd1963->currentFont.offset) * charHeigth) + 4;
    int             idx;
    int             i;
    unsigned char*  row         = ssd1963->currentFont.font + offset + charHeigth - 1;;

    ssd1963_setXY(x, y, x + 7, y + charHeigth);
    for (idx = 0; idx < charHeigth; idx++)
    {
        if ((*row & 0x80) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x40) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x20) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x10) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x08) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x04) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x02) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        if ((*row & 0x01) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%0"); asm("LD (HL),%FF");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } /* end if */
        row--;      
    } /* end for */
} /* end ssd1963_printCharFast_MAGENTA */


void ssd1963_printCharFast_BLACK(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y)
{
    int             charHeigth  = ssd1963->currentFont.y_size;
    int             offset      = ((character - ssd1963->currentFont.offset) * charHeigth) + 4;
    int             idx;
    int             i;
    unsigned char*  row         = ssd1963->currentFont.font + offset + charHeigth - 1;;

    ssd1963_setXY(x, y, x + 7, y + charHeigth);
    for (idx = 0; idx < charHeigth; idx++)
    {
        if ((*row & 0x80) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } /* end if */
        if ((*row & 0x40) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } /* end if */
        if ((*row & 0x20) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } /* end if */
        if ((*row & 0x10) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } /* end if */
        if ((*row & 0x08) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } /* end if */
        if ((*row & 0x04) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } /* end if */
        if ((*row & 0x02) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } /* end if */
        if ((*row & 0x01) != 0)
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%0"); asm("LD (HL),%0"); asm("LD (HL),%0");
        } 
        else
        {
            asm("LD	HL,(_writeDATA)");
            asm("LD (HL),%FF"); asm("LD (HL),%FF"); asm("LD (HL),%FF");
        } /* end if */
        row--;      
    } /* end for */
} /* end ssd1963_printCharFast_BLACK */


void ssd1963_printCharFast(SSD1963* ssd1963,
                                 unsigned char character,
                                 int x,
                                 int y,
                                 enum colorFast color)
{
    switch (color)
    {
        case WHITE:
            ssd1963_printCharFast_WHITE(ssd1963, character, x, y);
            break;
        case RED:
            ssd1963_printCharFast_RED(ssd1963, character, x, y);
            break;
        case GREEN:
            ssd1963_printCharFast_GREEN(ssd1963, character, x, y);
            break;
        case BLUE:
            ssd1963_printCharFast_BLUE(ssd1963, character, x, y);
            break;
        case YELLOW:
            ssd1963_printCharFast_YELLOW(ssd1963, character, x, y);
            break;
        case CYAN:
            ssd1963_printCharFast_CYAN(ssd1963, character, x, y);
            break;
        case MAGENTA:
            ssd1963_printCharFast_MAGENTA(ssd1963, character, x, y);
            break;
        case BLACK:
            ssd1963_printCharFast_BLACK(ssd1963, character, x, y);
            break;
        default:
            break;
    } /* end switch */
} /* end ssd1963_printCharFast */


void ssd1963_printFast(SSD1963* ssd1963,
                       char *string, 
                       int x, 
                       int y,
                       enum colorFast color)
{
	int stl;
    int i;

	stl = strlen(string);
	for (i=0; i<stl; i++)
		ssd1963_printCharFast_WHITE(ssd1963, (unsigned char)*string++, x + (i * 8), y);
} /* end ssd1963_printFast */ 

/* ************************************************************************ */
