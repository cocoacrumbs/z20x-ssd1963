#include <eZ80.h>
#include <stdio.h>
#include <stdlib.h>

#include "timer.h"
#include "ssd1963.h"

extern fontdatatype BBCFont[];
extern fontdatatype CP437font_8x16[];
extern fontdatatype SmallFont[];
extern fontdatatype BigFont[];
extern fontdatatype SevenSegNumFont[];

int main()
{
    SSD1963         mySSD1963;
    short           x           = 0;
    short           y           = 0;
    short           width       = 0;
    short           heigth      = 0;
    short           ctr         = 0;
    unsigned char   brightness  = 0;
    enum colorFast  color       = WHITE;

    timer2_init(1);             // Set to 1 ms interval
   
  	ssd1963_setup(&mySSD1963);

    while (1)
    {
        ssd1963_clrScr(&mySSD1963);

        ssd1963_setFrontColor(&mySSD1963, 255, 0, 0);
        ssd1963_setFont(&mySSD1963, BBCFont);
        ssd1963_print(&mySSD1963, "Hello World!!! (BBC Font 8*8)", (800/2) - ((29*8)/2), 222 + 16 + 8 + 8 + 12);

        ssd1963_setFrontColor(&mySSD1963, 0, 255, 0);
        ssd1963_setFont(&mySSD1963, SmallFont);
        ssd1963_print(&mySSD1963, "Hello World!!! (Small Font 8*12)", (800/2) - ((32*8)/2), 222 + 16 + 8);

        ssd1963_setFrontColor(&mySSD1963, 255, 255, 0);
        ssd1963_setFont(&mySSD1963, CP437font_8x16);
        ssd1963_print(&mySSD1963, "Hello World!!! (CP437 Font 8*16)", (800/2) - ((32*8)/2), 222);

        ssd1963_setFrontColor(&mySSD1963, 0, 0, 255);
        ssd1963_setFont(&mySSD1963, BigFont);
        ssd1963_print(&mySSD1963, "Hello World!!! (Big Font 16*16)", (800/2) - ((31*16)/2), 202);

        x          = 150;
        y          = 200;
        width      = 500;
        heigth     = 68 + 4 + 4;
        brightness = 10;

        for (ctr=0; ctr<100; ctr++)
        {
            ssd1963_setFrontColor(&mySSD1963, brightness, brightness, brightness);
            ssd1963_drawRectangle(&mySSD1963, x, y, width, heigth);

            x          = x - 1;
            y          = y - 2;
            width      = width + 2;
            heigth     = heigth + 4;
            brightness = brightness + 2;
        } /* end for */

        ssd1963_setFrontColor(&mySSD1963, 255, 0, 0);
        ssd1963_setFont(&mySSD1963, SevenSegNumFont);
        ssd1963_print(&mySSD1963, "0123456789", (800/2) - ((10*32)/2), 10);

        delayms(2000);
        ssd1963_clrScr(&mySSD1963);

        x = 0;
        while (x < 800)
        {
            ssd1963_setFrontColor(&mySSD1963, rand() % 256, rand() % 256, rand() % 256);
            ssd1963_drawLine(&mySSD1963, x, 0, 799 - x, 479);
            x = x + 10;
        } /* end while */

        y = 480;
        while (y > 0)
        {
            ssd1963_setFrontColor(&mySSD1963, rand() % 256, rand() % 256, rand() % 256);
            ssd1963_drawLine(&mySSD1963, 0, y, 799, 479 - y);
            y = y - 10;
        } /* end while */

        delayms(2000);
        ssd1963_clrScr(&mySSD1963);

        ssd1963_setFrontColor(&mySSD1963, 255, 0, 0);
        ssd1963_setFont(&mySSD1963, CP437font_8x16);
        ssd1963_print(&mySSD1963, "Rounded Rectangle", (800/2) - ((17*8)/2), 480/2 - 16/2);

        x          = 150;
        y          = 200;
        width      = 500;
        heigth     = 68 + 4 + 4;

        for (ctr=0; ctr<26; ctr++)
        {
            ssd1963_setFrontColor(&mySSD1963, rand() % 256, rand() % 256, rand() % 256);
            ssd1963_drawRoundedRectangle(&mySSD1963, x, y, width, heigth);

            x          = x - 4;
            y          = y - 8;
            width      = width + 8;
            heigth     = heigth + 16;
        } /* end for */

        delayms(2000);
        ssd1963_clrScr(&mySSD1963);

        x          = 150;
        y          = 200;
        width      = 500;
        heigth     = 68 + 4 + 4;

        for (ctr=0; ctr<10; ctr++)
        {
            ssd1963_setFrontColor(&mySSD1963, rand() % 256, rand() % 256, rand() % 256);
            ssd1963_fillRectangle(&mySSD1963, x, y, width, heigth);

            x          = x - 4;
            y          = y - 8;
            width      = width + 8;
            heigth     = heigth + 16;
        } /* end for */

        ssd1963_clrScr(&mySSD1963);

        x          = 150;
        y          = 200;
        width      = 500;
        heigth     = 68 + 4 + 4;

        for (ctr=0; ctr<10; ctr++)
        {
            ssd1963_setFrontColor(&mySSD1963, rand() % 256, rand() % 256, rand() % 256);
            ssd1963_fillRoundedRectangle(&mySSD1963, x, y, width, heigth);

            x          = x - 4;
            y          = y - 8;
            width      = width + 8;
            heigth     = heigth + 16;
        } /* end for */

        ssd1963_clrScr(&mySSD1963);        

        for (ctr=0; ctr<240; ctr = ctr + 10)
        {
            ssd1963_setFrontColor(&mySSD1963, rand() % 256, rand() % 256, rand() % 256);
            ssd1963_drawCircle(&mySSD1963, 400, 240, ctr);
        } /* end for */

        delayms(2000);
        ssd1963_clrScr(&mySSD1963);

        ssd1963_setFont(&mySSD1963, CP437font_8x16);
        for (x=0; x<64; x++)
        {
            ssd1963_printCharFast(&mySSD1963, (unsigned char)x, 144 + (x*8), 256, color);
            color++; color = color % 8;
        } /* end for */
        for (x=64; x<128; x++)
        {
            ssd1963_printCharFast(&mySSD1963, (unsigned char)x, 144 + (x-64)*8, 240, color);
            color++; color = color % 8;
        } /* end for */
        for (x=128; x<192; x++)
        {
            ssd1963_printCharFast(&mySSD1963, (unsigned char)x, 144 + (x-128)*8, 224, color);
            color++; color = color % 8;
        } /* end for */
        for (x=192; x<256; x++)
        {
            ssd1963_printCharFast(&mySSD1963, (unsigned char)x, 144 + (x-192)*8, 208, color);
            color++; color = color % 8;
        } /* end for */

        delayms(2000);
        ssd1963_clrScr(&mySSD1963);

        ssd1963_setFrontColor(&mySSD1963, 0, 255, 0);
        ssd1963_print(&mySSD1963, "[koen@manjaro-3900X CP437-8x16]$ ls -al /",                       0, 464);
        ssd1963_print(&mySSD1963, "total 276",                                                       0, 448);
        ssd1963_print(&mySSD1963, "drwxr-xr-x  17 root root   4096 Apr 15 09:53 .",                  0, 432);
        ssd1963_print(&mySSD1963, "drwxr-xr-x  17 root root   4096 Apr 15 09:53 ..",                 0, 416);
        ssd1963_print(&mySSD1963, "lrwxrwxrwx   1 root root      7 Jan 20 17:33 bin -> usr/bin",     0, 400);
        ssd1963_print(&mySSD1963, "drwxr-xr-x   5 root root   4096 Jun 30 09:15 boot",               0, 384);
        ssd1963_print(&mySSD1963, "-rw-r--r--   1 root root  15500 Oct 19  2020 desktopfs-pkgs.txt", 0, 368);
        ssd1963_print(&mySSD1963, "drwxr-xr-x  23 root root   4360 Jul  9 16:01 dev",                0, 352);
        ssd1963_print(&mySSD1963, "drwxr-xr-x  95 root root   4096 Jul  9 10:06 etc",                0, 336);
        ssd1963_print(&mySSD1963, "-rw-r--r--   1 root root 169993 Apr 22 11:07 file",               0, 320);
        ssd1963_print(&mySSD1963, "drwxr-xr-x   4 root root   4096 Sep 26  2020 home",               0, 304);
        ssd1963_print(&mySSD1963, "lrwxrwxrwx   1 root root      7 Jan 20 17:33 lib -> usr/lib",     0, 288);
        ssd1963_print(&mySSD1963, "lrwxrwxrwx   1 root root      7 Jan 20 17:33 lib64 -> usr/lib",   0, 272);
        ssd1963_print(&mySSD1963, "drwx------   2 root root  16384 Nov 25  2020 lost+found",         0, 256);
        ssd1963_print(&mySSD1963, "-rw-r--r--   1 root root      7 Oct 19  2020 .manjaro-tools",     0, 240);
        ssd1963_print(&mySSD1963, "drwxr-xr-x   2 root root   4096 Sep 26  2020 mnt",                0, 224);
        ssd1963_print(&mySSD1963, "drwxr-xr-x   5 root root   4096 Nov 25  2020 opt",                0, 208);
        ssd1963_print(&mySSD1963, "dr-xr-xr-x 546 root root      0 Jun 29 16:39 proc",               0, 192);
        ssd1963_print(&mySSD1963, "drwxr-x---   7 root root   4096 Nov 25  2020 root",               0, 176);
        ssd1963_print(&mySSD1963, "-rw-r--r--   1 root root   5136 Oct 19  2020 rootfs-pkgs.txt",    0, 160);
        ssd1963_print(&mySSD1963, "drwxr-xr-x  29 root root    720 Jul  9 08:43 run",                0, 144);
        ssd1963_print(&mySSD1963, "lrwxrwxrwx   1 root root      7 Jan 20 17:33 sbin -> usr/bin",    0, 128);
        ssd1963_print(&mySSD1963, "drwxr-xr-x   4 root root   4096 Oct 19  2020 srv",                0, 112);
        ssd1963_print(&mySSD1963, "dr-xr-xr-x  13 root root      0 Jun 29 16:39 sys",                0, 96);
        ssd1963_print(&mySSD1963, "drwxrwxrwt  21 root root    520 Jul  9 17:15 tmp",                0, 80);
        ssd1963_print(&mySSD1963, "drwxr-xr-x   9 root root   4096 Jul  9 10:06 usr",                0, 64);
        ssd1963_print(&mySSD1963, "drwxr-xr-x  12 root root   4096 Jun 29 16:39 var",                0, 48);
        ssd1963_print(&mySSD1963, "[koen@manjaro-3900X CP437-8x16]$",                                0, 32);

        delayms(2000);
        ssd1963_clrScr(&mySSD1963);

        ssd1963_printFast(&mySSD1963, "[koen@manjaro-3900X CP437-8x16]$ ls -al /",                       0, 464, WHITE);
        ssd1963_printFast(&mySSD1963, "total 276",                                                       0, 448, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x  17 root root   4096 Apr 15 09:53 .",                  0, 432, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x  17 root root   4096 Apr 15 09:53 ..",                 0, 416, WHITE);
        ssd1963_printFast(&mySSD1963, "lrwxrwxrwx   1 root root      7 Jan 20 17:33 bin -> usr/bin",     0, 400, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x   5 root root   4096 Jun 30 09:15 boot",               0, 384, WHITE);
        ssd1963_printFast(&mySSD1963, "-rw-r--r--   1 root root  15500 Oct 19  2020 desktopfs-pkgs.txt", 0, 368, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x  23 root root   4360 Jul  9 16:01 dev",                0, 352, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x  95 root root   4096 Jul  9 10:06 etc",                0, 336, WHITE);
        ssd1963_printFast(&mySSD1963, "-rw-r--r--   1 root root 169993 Apr 22 11:07 file",               0, 320, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x   4 root root   4096 Sep 26  2020 home",               0, 304, WHITE);
        ssd1963_printFast(&mySSD1963, "lrwxrwxrwx   1 root root      7 Jan 20 17:33 lib -> usr/lib",     0, 288, WHITE);
        ssd1963_printFast(&mySSD1963, "lrwxrwxrwx   1 root root      7 Jan 20 17:33 lib64 -> usr/lib",   0, 272, WHITE);
        ssd1963_printFast(&mySSD1963, "drwx------   2 root root  16384 Nov 25  2020 lost+found",         0, 256, WHITE);
        ssd1963_printFast(&mySSD1963, "-rw-r--r--   1 root root      7 Oct 19  2020 .manjaro-tools",     0, 240, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x   2 root root   4096 Sep 26  2020 mnt",                0, 224, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x   5 root root   4096 Nov 25  2020 opt",                0, 208, WHITE);
        ssd1963_printFast(&mySSD1963, "dr-xr-xr-x 546 root root      0 Jun 29 16:39 proc",               0, 192, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-x---   7 root root   4096 Nov 25  2020 root",               0, 176, WHITE);
        ssd1963_printFast(&mySSD1963, "-rw-r--r--   1 root root   5136 Oct 19  2020 rootfs-pkgs.txt",    0, 160, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x  29 root root    720 Jul  9 08:43 run",                0, 144, WHITE);
        ssd1963_printFast(&mySSD1963, "lrwxrwxrwx   1 root root      7 Jan 20 17:33 sbin -> usr/bin",    0, 128, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x   4 root root   4096 Oct 19  2020 srv",                0, 112, WHITE);
        ssd1963_printFast(&mySSD1963, "dr-xr-xr-x  13 root root      0 Jun 29 16:39 sys",                0, 96, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxrwxrwt  21 root root    520 Jul  9 17:15 tmp",                0, 80, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x   9 root root   4096 Jul  9 10:06 usr",                0, 64, WHITE);
        ssd1963_printFast(&mySSD1963, "drwxr-xr-x  12 root root   4096 Jun 29 16:39 var",                0, 48, WHITE);
        ssd1963_printFast(&mySSD1963, "[koen@manjaro-3900X CP437-8x16]$",                                0, 32, WHITE);

        delayms(2000);
    } /* end while */

    return 0;
} /* end main */
