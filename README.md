# SSD1963 interface code for the eZ80 based Z20X computer

![picture](./Images/screenshot.jpg)

A minimal set of routines to initialize the **SSD1963 LCD Display Controller** 
in combination with a 7" LCD Display as used by the 
[**Z20X computer**](<http://z20x.computer/>).

Inspiration has been taken from the [**UTFT Library**](<http://www.rinkydinkelectronics.com/library.php?id=51>).

Currently, this library allows the following:

* Initialize the SSD1963 LCD Display Controller for a 7" LCD display.
* Set front/background color.
* Clear the screen (black background).
* Draw horizontal, vertical lines and rectangles.
* Select a font (default fonts from the  [**UTFT Library**](<http://www.rinkydinkelectronics.com/library.php?id=51>) 
  and an attempt to recreate the BBC font).
* Print a character/string in any color and position on the screen.
* Set the Gamma curve.

## Caveats

Drawing speed is rather slow due to the huge amount of bytes that need 
to be send to the screen. For every pixel we need to send 3 bytes (one 
byte each for Red, Green and Blue). 

* For a 800 by 480 LCD screen, this means that 800 * 480 * 3 = 1,152,000 bytes 
  (> 1 MByte !) needs to be written.
* For a 8 by 8 pixel character, this means that 8 * 8 * 3 = 192 bytes 
  needs to be written to the LCD controller for just one character.

The library is hardcoded for landscape mode. This keeps the code cleaner 
and easier to understand when adding new functionality.

Interfacing to the SSD1963 LCD Display Controller is achieved as memory
mapped IO. The library is currently hardcoded to use these adresses:

* ``0x400000`` to access the command register
* ``0x400001`` to access the data register
You need to setup CS1 for the 0x400000 to 0x400001 memory region since 
the Z20X computer has the CS1 wired to the SSD1963 LCD Display Controller.


[UPDATE July 10, 2021]

* bug fix which now allows to use all 256 characters for the print 
  routines.
* Short's have been replaced by int's for faster code (the C compiler 
  internally always converts short's to int's producing slower code).
* The BBC font problems have been fixed (some characters had the wrong 
  ASCII code).
* A new 8*16 pixel font, CP437, has been added containing the full 256 
  character set.
* Code has been added for faster printing at the cost of some flexibility.
  
  * The font has to be 8 pixels wide.
  * For/background color settings are ignored and color choices are 
    limited to 8 colors.

* A new video has been uploaded showcasing the new capabilities and can 
  be found [**here**](<https://www.youtube.com/watch?v=x1ti01ocxG4>).

[UPDATE August 19, 2021]

* Added the following graphic functions:

    * ``ssd1963_drawPixel``
    * ``ssd1963_drawLine``
    * ``ssd1963_drawRoundedRectangle``
    * ``ssd1963_fillRectangle``
    * ``ssd1963_fillRoundedRectangle``
    * ``ssd1963_drawCircle``
